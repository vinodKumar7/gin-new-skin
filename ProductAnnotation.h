//
//  ProductAnnotation.h
//  GrabItNow
//
//  Created by Monish Kumar on 07/01/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MKMapview.h>
#import <CoreLocation/CoreLocation.h>
#import "Product.h"

@interface ProductAnnotation : NSObject<MKAnnotation>

@property (nonatomic, readwrite) CLLocationCoordinate2D coordinate;

// Title and subtitle for use by selection UI.
@property (nonatomic, readwrite, copy) NSString *title;
@property (nonatomic, readwrite, copy) NSString *subtitle;
@property (nonatomic,strong) Product *prod;
@property (nonatomic, readwrite) NSInteger prodAnnTag;
@property (nonatomic, readwrite, copy) NSString *pinColor;
//- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate;

@end
