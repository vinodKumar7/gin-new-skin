//
//  HomeViewController.h
//  GrabItNow
//
//  Created by MyRewards on 11/24/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//
// MOTOR ONE
#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "ProductViewController.h"
#import "NoticeBoardDataParser.h"
#import "ZBarSDK.h"
#import <AVFoundation/AVFoundation.h>

typedef enum {
    MenuItemSearch,
    MenuItemMyfavorites,
    MenuItemNearbyme,
    MenuItemHotOffers,
    MenuItemDailyDeal,
    MenuItemMyCard,
    MenuItemHelp
}MenuItem;

@interface HomeViewController : UIViewController <ASIHTTPRequestDelegate, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, NoticeBoardXMLParserDelegate,ZBarReaderViewDelegate,AVCaptureMetadataOutputObjectsDelegate>
{
    IBOutlet UIView *menuView;
    IBOutlet UIView *headerView;
    IBOutlet UIView *bannerView;
    IBOutlet UIImageView *bannerImageView;
    
    IBOutlet UIButton *menuButton;
    
    IBOutlet UIButton *backButton;
    
    IBOutlet UILabel *headerLabel;
    
    IBOutlet UIButton *menuButton1;
    IBOutlet UIButton *menuButton2;
    IBOutlet UIButton *menuButton3;
    IBOutlet UIButton *menuButton4;
    IBOutlet UIButton *menuButton5;
    IBOutlet UIButton *menuButton6;
    
    UITableView *menuTable;
    
    UIImage *bannerImage;
    
}

@property (nonatomic, strong) IBOutlet UITableView *menuTable;

@property (nonatomic, strong) IBOutlet UIView *menuView;
@property (nonatomic, strong) IBOutlet UIView *headerView;
@property (nonatomic, strong) IBOutlet UIView *bannerView;
@property (nonatomic, strong) IBOutlet UIButton *menuButton;
@property (nonatomic, strong) IBOutlet UIImageView *bannerImageView;
@property (nonatomic, strong) IBOutlet UIButton *backButton;
@property (nonatomic, strong) IBOutlet UILabel *headerLabel;

@property (nonatomic, strong) IBOutlet UIButton *menuButton1;
@property (nonatomic, strong) IBOutlet UIButton *menuButton2;
@property (nonatomic, strong) IBOutlet UIButton *menuButton3;
@property (nonatomic, strong) IBOutlet UIButton *menuButton4;
@property (nonatomic, strong) IBOutlet UIButton *menuButton5;
@property (nonatomic, strong) IBOutlet UIButton *menuButton6;

@property (nonatomic, strong) UITextField *currentTextfield;
@property (nonatomic, strong) UIImage *bannerImage;
@property (nonatomic, strong) NSString *menuTitleString;

- (void) showBlackTransparentView;
- (void) removeBlackTransparentView;
-(void) calculatingUnreadNotices;
- (void) hideBackButon:(BOOL) hide;
- (IBAction)backButtonTapped:(id)sender;

- (void) setHeaderTitle:(NSString *) headerTitle;

- (IBAction) menuButtonTapped:(id) sender;
- (IBAction)menuItemButtonTapped:(id)sender;
- (void)showMailComposer:(NSArray *)recepients withBody:(NSString *)text;
- (void)showMailComposer;
@end
