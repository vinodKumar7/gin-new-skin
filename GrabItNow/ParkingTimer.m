//
//  ParkingTimer.m
//  TWU2
//
//  Created by vairat on 11/04/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import "ParkingTimer.h"

@implementation ParkingTimer

@synthesize parkedLocation;
@synthesize parkedTime;
@synthesize latitude;
@synthesize longitude;
@synthesize location;
@synthesize when;
@end
