//
//  ScannerViewController.h
//  CWE
//
//  Created by vairat on 22/08/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBarSDK.h"
@interface ScannerViewController : UIViewController< ZBarReaderDelegate >
@property (retain, nonatomic) IBOutlet UITextView *mytextView;
@property (retain, nonatomic) IBOutlet UIImageView *myImageView;
@property (strong, nonatomic) IBOutlet UIButton *scan_Button;

- (IBAction)Scan_Button_Action:(id)sender;
- (IBAction)closeWindow:(id)sender;

@end
