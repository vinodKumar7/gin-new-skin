//
//  NSString_stripHtml.h
//  GrabItNow
//
//  Created by MyRewards on 3/21/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <Foundation/Foundation.h>

//@interface NSString_stripHtml : NSObject
//
//@end

@interface NSString (stripHtml)
- (NSString*)stripHtml;
@end