//
//  NoticeBoard.m
//  GrabItNow
//
//  Created by MyRewards on 3/25/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import "NoticeBoard.h"

@implementation NoticeBoard


@synthesize  notice_Id;
@synthesize  user_Id;
@synthesize  client_Id;
@synthesize  subject;
@synthesize  details;
@synthesize  created_Date;
@synthesize  start;
@synthesize  end;
@synthesize  sort;
@synthesize  active;
@synthesize index;

@end
